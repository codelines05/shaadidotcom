/* eslint-disable no-unused-vars */
import AboutBox from "./Home/AboutBox/AboutBox";
import HeaderWrapper from "./Header/HeaderWrapper";
import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import MatchListBox from "./Matches/MatchListBox/MatchListBox";
import MatchViewBox from "./Matches/MatchViewBox/MatchViewBox";

export {
    AboutBox, HeaderWrapper, Header, Footer, MatchListBox, MatchViewBox
}
