/* eslint-disable no-unused-vars */
import React from 'react'
import "./MatchListBoxMobi.css"
import { Carousel } from "react-bootstrap"

const MatchListBoxMobi = (props) => {

    const { user_name } = props;

    // const PhotoTempRequest = `assets/images/profiles/dummy/250-photo-request-f.jpg`
    const PhotoTempRequest = `assets/images/profiles/dummy/250-photo-profile-1.jpg`

    return (
        <>
            <div className='matchlistbox_mobi_wrapper'>
                <div className="matchlistbox_mobi">
                    <div className="matchlistbox_mobi_image_box">
                        <Carousel fade interval={3000} className='matchlistbox_mobi_carousel'>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100 matchlistbox_mobi_carousel_image"
                                    src={`${process.env.PUBLIC_URL}/${PhotoTempRequest}`}
                                    alt=""
                                />
                            </Carousel.Item>
                            {/* <Carousel.Item>
                                <img
                                    className="d-block w-100 matchlistbox_mobi_carousel_image"
                                    src={`${process.env.PUBLIC_URL}/${PhotoTempRequest}`}
                                    alt=""
                                />
                            </Carousel.Item> */}
                        </Carousel>
                    </div>
                </div>
            </div>
        </>
    )
}

export default MatchListBoxMobi