import React from 'react'
import { Header, Footer, MatchListBox } from '../../../Components'
import {
    UncontrolledAccordion,
    AccordionItem,
    AccordionHeader,
    AccordionBody
} from "reactstrap"
import "./NewMatches.css"

const NewMatches = (props) => {

    return (
        <>
            <Header isProfile={true} />
            <div className="my_matches_page py-5">
                <div className="container_border">
                    <div className="row">
                        <div className="col-12 col-sm-12 col-md-1">

                        </div>
                        <div className="col-12 col-sm-12 col-md-9">
                            <MatchListBox user_name="Jafria" />
                            <MatchListBox user_name="Asma" />
                            <MatchListBox user_name="Zaffra" />
                            <MatchListBox />
                            <MatchListBox />
                            <MatchListBox />
                            <MatchListBox />
                            <MatchListBox />
                            <MatchListBox />
                            <MatchListBox />
                            <MatchListBox />
                            <MatchListBox />
                            <MatchListBox />
                            <MatchListBox />
                        </div>
                        <div className="col-12 col-sm-12 col-md-2">
                            <div className="my_matches_filter_wrapper py-3">
                                <div className="my_matches_filter_box">
                                    <UncontrolledAccordion
                                        defaultOpen={[
                                            '1',
                                            '2',
                                            '3',
                                            '4',
                                            '5'
                                        ]}
                                        stayOpen
                                    >
                                        <AccordionItem className='py-1 px-3' style={{ backgroundColor: 'var(--lighter-border)' }}>
                                            <p className='mb-0' style={{ fontWeight: 700, }}>Filter My Matches</p>
                                        </AccordionItem>
                                        <AccordionItem>
                                            <AccordionHeader targetId="1">
                                                Refine-Option 1
                                            </AccordionHeader>
                                            <AccordionBody accordionId="1">
                                                <input type="radio" name="fav_language_1" value="HTML" />
                                                <label for="html" className='ms-1'>Option -1</label><br />
                                                <input type="radio" name="fav_language_1" value="CSS" />
                                                <label for="css" className='ms-1'>Option -2</label><br />
                                                <input type="radio" name="fav_language_1" value="JavaScript" />
                                                <label for="javascript" className='ms-1'>Option -3</label>
                                            </AccordionBody>
                                        </AccordionItem>
                                        <AccordionItem>
                                            <AccordionHeader targetId="2">
                                                Refine-Option 2
                                            </AccordionHeader>
                                            <AccordionBody accordionId="2">
                                                <input type="radio" name="fav_language_2" value="HTML" />
                                                <label for="html" className='ms-1'>Option -1</label><br />
                                                <input type="radio" name="fav_language_2" value="CSS" />
                                                <label for="css" className='ms-1'>Option -2</label><br />
                                                <input type="radio" name="fav_language_2" value="JavaScript" />
                                                <label for="javascript" className='ms-1'>Option -3</label>
                                            </AccordionBody>
                                        </AccordionItem>
                                        <AccordionItem>
                                            <AccordionHeader targetId="3">
                                                Refine-Option 3
                                            </AccordionHeader>
                                            <AccordionBody accordionId="3">
                                                <input type="radio" name="fav_language_3" value="HTML" />
                                                <label for="html" className='ms-1'>Option -1</label><br />
                                                <input type="radio" name="fav_language_3" value="CSS" />
                                                <label for="css" className='ms-1'>Option -2</label><br />
                                                <input type="radio" name="fav_language_3" value="JavaScript" />
                                                <label for="javascript" className='ms-1'>Option -3</label>
                                            </AccordionBody>
                                        </AccordionItem>
                                        <AccordionItem>
                                            <AccordionHeader targetId="4">
                                                Refine-Option 4
                                            </AccordionHeader>
                                            <AccordionBody accordionId="4">
                                                <input type="radio" name="fav_language_4" value="HTML" />
                                                <label for="html" className='ms-1'>Option -1</label><br />
                                                <input type="radio" name="fav_language_4" value="CSS" />
                                                <label for="css" className='ms-1'>Option -2</label><br />
                                                <input type="radio" name="fav_language_4" value="JavaScript" />
                                                <label for="javascript" className='ms-1'>Option -3</label>
                                            </AccordionBody>
                                        </AccordionItem>
                                        <AccordionItem>
                                            <AccordionHeader targetId="5">
                                                Refine-Option 5
                                            </AccordionHeader>
                                            <AccordionBody accordionId="5">
                                                <input type="radio" name="fav_language_5" value="HTML" />
                                                <label for="html" className='ms-1'>Option -1</label><br />
                                                <input type="radio" name="fav_language_5" value="CSS" />
                                                <label for="css" className='ms-1'>Option -2</label><br />
                                                <input type="radio" name="fav_language_5" value="JavaScript" />
                                                <label for="javascript" className='ms-1'>Option -3</label>
                                            </AccordionBody>
                                        </AccordionItem>
                                    </UncontrolledAccordion>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default NewMatches